import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

interface Rankable {
  rank?:number
}

/*
  Generated class for the BubbleSortProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class BubbleSortProvider {

  constructor() {
  }

  numSwapsRequired(data:Rankable[]) : number {
    let mydata = data.map( 
      item => { return {rank:item.rank} } 
    );
    let numSwaps = 0;
    while( this.doNextSwap(mydata)!=null ) {
      numSwaps++;
    }
   return numSwaps;
  }

  // This returns the data after one swap.  
  //  Or if no swaps are required, then null is returned.
  doNextSwap( data:Rankable[] ) {
    for( let i=data.length-1; i>=1; i--) {
      let j = i-1
      let di = data[i]
      let dj = data[j]
      if ( di.rank < dj.rank ) {
        data[i] = dj
        data[j] = di
        return data
      }
    }
    return null
  }

}
