import { Injectable } from '@angular/core';

@Injectable() 
export class SwapScheduler{

    nSwaps:number;
    durationMs:number;
    nSwapsSoFar:number;
    constructor(){}

    schedule(nSwaps:number,durationMs:number) {
        this.nSwaps = nSwaps;
        this.durationMs = durationMs;
        this.nSwapsSoFar = 0
    }

    isSwapRequiredNow(currentMs:number) : boolean {
        let expectedSwaps = this.expectedSwapsAtThisTime(currentMs)
        let isSwapRequired =  this.nSwapsSoFar < expectedSwaps
        if (isSwapRequired)
            this.nSwapsSoFar++
        return isSwapRequired
    }

    private expectedSwapsAtThisTime(currentMs:number) : number {
        return (currentMs*this.nSwaps) / this.durationMs;
    }


}
