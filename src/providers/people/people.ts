import { ShuffleProvider } from './../shuffle/shuffle';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

export interface Person {
  name:string,
  email?:string
  rank?:number
}

interface Group {
  groupName: string,
  people: Person[]
}

let theBears = [
    {name:"Baby"},
    {name:"Mom"},
    {name:"Pop"},
    {name:"Goldy"},
  ];

let theEnchanted = [
  {name: "Doc"},
  {name: "Doppy"},
  {name: "Grumpy"},
  {name: "Slimy"},
  {name: "Smelly"},
  {name: "Happy"},
  {name: "Inky"},
  {name: "Blinky"},
  {name: "Clyde"},
  {name: "Snow"},
]

let groups : Group[] = [
  {
    groupName: "Enchanted",
    people: theEnchanted
  },
  {
    groupName: "The Bears",
    people: theBears
  }
];


/*
  Generated class for the PeopleProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class PeopleProvider {

  constructor( private shuffleSvc: ShuffleProvider ) {
    console.log('Hello PeopleProvider Provider');
  }

  getPeople( groupName: string ) : Observable<Person[]> {
    let group = groups.find( gr => gr.groupName === groupName )
    return Observable.of( group ? group.people : [] );
  }

  getGroupNames() : Observable<string[]> {
    let groupNames = groups.map( 
      gr => gr.groupName
    )   
    return Observable.of( groupNames ); 
  }

  assignShuffledRanksToPeople( people : Person[] ) : Person[] {
    var ranks = this.shuffleSvc.createShuffledRanks1ToN(people.length); 
    for (let i = 0; i<people.length; i++ ) {
      people[i].rank = ranks[i];
    }
    return people;
  }

}
