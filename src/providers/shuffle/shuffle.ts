import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';

/*
  Generated class for the ShuffleProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ShuffleProvider {

  constructor() {
  }

  createShuffledRanks1ToN( n: number ) : number[] {
    let data = this.createRanks1ToN(n);
    return this.shuffleArray(data);
  }

  private createRanks1ToN( n: number ) : number[] {
    let data = [];
    for( let i=1; i<=n; i++ ) {
      data.push(i);
    }
    return data;
  }

  shuffleArray(data: any[]) : any[] {
    let currentIndex = data.length;
    while( 0 !== currentIndex ) {
      let randomIndex = Math.floor( Math.random() * currentIndex );
      currentIndex -= 1;
      var t = data[currentIndex];
      data[currentIndex] = data[randomIndex];
      data[randomIndex] = t;
    }
    return data;
  }


}
