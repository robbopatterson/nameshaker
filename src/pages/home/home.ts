import { SwapScheduler } from './../../providers/bubble-sort/swap-schedule';
import { Subscription } from 'rxjs/Subscription';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { ShuffleProvider } from './../../providers/shuffle/shuffle';
import { PeopleProvider, Person } from './../../providers/people/people';
import { BubbleSortProvider } from './../../providers/bubble-sort/bubble-sort'
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  people : Person[];
  timerSub:Subscription;

  constructor(
    public navCtrl: NavController, 
    private navParams: NavParams, 
    private peopleSvc: PeopleProvider,
    private shuffleSvc: ShuffleProvider,
    private bubbleSortSvc: BubbleSortProvider,
    private swapScheduler: SwapScheduler,
    private loadingController: LoadingController
    ) {
  }

  ionViewDidLoad() {
    var group = this.navParams.get('group');
    this.peopleSvc.getPeople(group).subscribe(
      peopleList => this.people = peopleList
    )
  }

  shakeIt() {
    if (this.timerSub==null) {
      let audio = new Audio('assets/sounds/prize-wheel.mp3')
      let loading = this.loadingController.create({
        content: "Shaking it..."
      })
      audio.play()
      this.people = this.shuffleSvc.shuffleArray(this.people);
      this.people = this.peopleSvc.assignShuffledRanksToPeople(this.people);
      let swapsLeft = this.bubbleSortSvc.numSwapsRequired(this.people);
      this.swapScheduler.schedule(swapsLeft,3750)
      let data = null
      loading.present()
      let startMs = Date.now()
      let timer = TimerObservable.create(0,100)
      this.timerSub = timer.subscribe( _ => {
        let elapsedMs = Date.now() - startMs

        while( this.swapScheduler.isSwapRequiredNow(elapsedMs) ) {
          if ( swapsLeft-- > 0 ) {
            data=this.bubbleSortSvc.doNextSwap(this.people)
            this.people = data
          } else if (this.timerSub !=null) {
            this.timerSub.unsubscribe();
            this.timerSub = null;
            loading.dismiss()
          }
        }
        
      } )
    }
  }
}
