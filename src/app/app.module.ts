import { SwapScheduler } from './../providers/bubble-sort/swap-schedule';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { PeopleProvider } from '../providers/people/people';
import { ShuffleProvider } from '../providers/shuffle/shuffle';
import { BubbleSortProvider } from '../providers/bubble-sort/bubble-sort';

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    PeopleProvider,
    ShuffleProvider,
    BubbleSortProvider,
    SwapScheduler
  ]
})
export class AppModule {}
